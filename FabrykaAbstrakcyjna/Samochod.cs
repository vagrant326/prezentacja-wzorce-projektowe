﻿using System;
namespace FabrykaAbstrakcyjna
{
    /// <summary>
    /// Klasa abstrakcyjna tworząca interfejs który musi zostać zaimplemetowany
    /// przez każdy obiekt typu Samochód. We wzorcu nazywany ProductA, Product1 itp.
    /// </summary>
    public abstract class Samochod
    {
        public string marka = "Suzuki";

        /// <summary>
        /// Jedyne pole które będzie ulegać zmianom
        /// </summary>
        public abstract string model { get; }

        /// <summary>
        /// Przykładowe pole które rozróżnia produkty
        /// </summary>
        public int iloscKol = 4;
        public int rocznik = DateTime.Now.Year;
    }
}
