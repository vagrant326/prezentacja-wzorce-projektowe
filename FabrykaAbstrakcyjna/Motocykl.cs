﻿using System;
namespace FabrykaAbstrakcyjna
{
    /// <summary>
    /// Klasa abstrakcyjna tworząca interfejs który musi zostać zaimplemetowany
    /// przez każdy obiekt typu Motocykl. We wzorcu nazywany ProductB, Product2 itp.
    /// </summary>
    public abstract class Motocykl
    {
        public string marka = "Suzuki";

        /// <summary>
        /// Jedyne pole które będzie ulegać zmianom
        /// </summary>
        public abstract string model { get; }

        /// <summary>
        /// Przykładowe pole które rozróżnia produkty
        /// </summary>
        public int iloscKol = 2;
        public int rocznik = DateTime.Now.Year;
    }
}
