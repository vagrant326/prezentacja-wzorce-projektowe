﻿using System;
namespace FabrykaAbstrakcyjna
{
    class Program
    {
        static void Main(string[] args)
        {
            // Utwórz nowy obiekt Concrete Creator
            Fabryka fab = new MagyarSuzukiCorp();

            // Wyślij zamówienie na nowy samochód do fabryki fab
            Samochod auto = Producent.WyprodukujSamochod(fab);

            // Wyślij zamówienie na nowy motocykl do fabryki fab
            Motocykl moto = Producent.WyprodukujMotocykl(fab);


            // Zmień fabrykę
            fab = new CambodiaSuzukiMotor();

            // Utwórz inny samochód
            auto = Producent.WyprodukujSamochod(fab);

            // Utwórz inny motocykl
            moto = Producent.WyprodukujMotocykl(fab);

            Console.ReadLine();
        }
    }
}
