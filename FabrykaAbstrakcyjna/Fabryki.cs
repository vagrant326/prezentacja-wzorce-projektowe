﻿using System;

namespace FabrykaAbstrakcyjna
{ 
    /* Plik zawierający 2 przykładowe implementacje interfejsu IFabryka.
     * Każda z nich pozwala wybrać spośród 2 modeli samochodów implementujących interfejs Samochod.
     * We wzorcu nazywane Concrete Factory lub Concrete Creator.
     */
    class MagyarSuzukiCorp : Fabryka
    {
        public override Samochod WyprodukujSamochod(string nazwa)
        {
            if (nazwa == "Vitara")
            {
                // Wyprodukuj Suzuki Vitara
                return new Vitara();
            }
            if (nazwa == "Wagon R+")
            {
                // Wyprodukuj Suzuki Wagon R+
                return new WagonRPlus();
            }
            Console.WriteLine("Zla nazwa");
            return null;
        }

        public override Motocykl WyprodukujMotocykl(string nazwa)
        {
            if (nazwa == "GSX 100")
            {
                // Wyprodukuj Suzuki GSX 100
                return new GSX100();
            }
            if (nazwa == "RM 60")
            {
                // Wyprodukuj Suzuki RM 60
                return new RM60();
            }
            Console.WriteLine("Zla nazwa");
            return null;
        }
    }

    class CambodiaSuzukiMotor : Fabryka
    {
        public override Samochod WyprodukujSamochod(string nazwa)
        {
            if (nazwa == "Samurai")
            {
                return new Samurai();
            }
            if(nazwa=="Swift")
            {
                return new Swift();
            }
            Console.WriteLine("Zla nazwa");
            return null;
        }

        public override Motocykl WyprodukujMotocykl(string nazwa)
        {
            if (nazwa == "DL 1000")
            {
                return new DL1000();
            }
            if (nazwa == "Intruder 1500")
            {
                return new Intruder1500();
            }
            Console.WriteLine("Zla nazwa");
            return null;
        }
    }
}
