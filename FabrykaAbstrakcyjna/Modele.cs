﻿namespace FabrykaAbstrakcyjna
{
    /* Plik zawierający różne modele Suzuki implementujące interfejs Samochód i Motocykl.
     * Zmienia się tylko przykładowe pole ale bez problemu można zmieniać ich wiele.
     */
    class WagonRPlus : Samochod
    {
        public override string model
        {
            get { return "Wagon R+"; }
        }
    }

    class Vitara : Samochod
    {
        public override string model
        {
            get { return "Vitara"; }
        }
    }
    class Samurai : Samochod
    {
        public override string model
        {
            get { return "Samurai"; }
        }
    }
    class Swift : Samochod
    {
        public override string model
        {
            get { return "Swift"; }
        }
    }

    class GSX100 : Motocykl
    {
        public override string model
        {
            get { return "GSX 100"; }
        }
    }
    class DL1000 : Motocykl
    {
        public override string model
        {
            get { return "DL 1000"; }
        }
    }
    class Intruder1500 : Motocykl
    {
        public override string model
        {
            get { return "Intruder 1500"; }
        }
    }
    class RM60 : Motocykl
    {
        public override string model
        {
            get { return "RM 60"; }
        }
    }
}
