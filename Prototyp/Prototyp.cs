﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace Prototyp
{
    /// <summary>
    /// Klasa abstrakcyjna tworząca interfejs wymagany dla wzorca Prototype.
    /// </summary>
    /// <typeparam name="T">Dowolny typ</typeparam>
    [Serializable()] // Wymagane
    abstract class IPrototyp<T>
    {
        /// <summary>
        /// Zwraca Shallow Copy. Wystarczające jeśli typ T zawiera wyłącznie pola typu prostego (int,string itp.).
        /// </summary>
        /// <returns>Skopiowany obiekt (pracownik)</returns>
        public T Kopiuj()
        {
            return (T)MemberwiseClone();
        }

        /// <summary>
        /// Wykorzystuje serializację by zwrócić Deep Copy. Wymagane jeśli klasa zawiera pola typu referencyjnego (np. klas)
        /// </summary>
        /// <returns>Zklonowany obiekt (pracownik)</returns>
        public T Klonuj()
        {
            MemoryStream strumien = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(strumien, this);
            strumien.Seek(0, SeekOrigin.Begin);
            T klon = (T) formatter.Deserialize(strumien);
            strumien.Close();
            return klon;
        }
    }

}
