﻿using System;
using System.Collections.Generic;

namespace Prototyp
{
    class Program
    {
        static void Main(string[] args)
        {
            /* W tym przykładzie używamy wzorca Prototype, ponieważ inicjalizacja obiektu wymaga
             * podania wielu danych. Dobrym powodem jest także np. duży nakład obliczeniowy
             * lub czasochłonne operacje.
             */

            List<Pracownik> listaPracownikow = new List<Pracownik>();
            listaPracownikow = Pracownik.PrzygotujDane(); // Może zostać usunięte

            // Stwórz nowego pracownika
            Console.WriteLine("Stworz nowego pracownika");
            Pracownik nowy = new Pracownik();
            listaPracownikow.Add(nowy);

            // Stwórz nowego pracownika na pdstw. prototypu
            Console.WriteLine("Stworz nowego pracownika");
            Console.WriteLine("Podaj stanowisko:");
            string stanowisko = Console.ReadLine();
            Pracownik nowszy = Pracownik.StworzPracownika(stanowisko);
            listaPracownikow.Add(nowszy);

            // Wypisywanie
            Console.Clear();
            foreach (Pracownik pracownik in listaPracownikow)
            {
                Pracownik.WypiszPracownika(pracownik);
            }
            Console.ReadLine();
        }
    }
}
