﻿using System;
using System.Collections.Generic;

namespace Prototyp
{
    /// <summary>
    /// Klasa opisująca pracownika
    /// </summary>
    class Pracownik : IPrototyp<Pracownik>
    {
        public string imie;
        public string nazwisko;

        #region Uprawnienia
        public bool salaA1;
        public bool salaA2;
        public bool salaA3;
        public bool salaA4;
        public bool salaA5;
        #endregion

        /// <summary>
        /// Funkcja zwracająca listę 3 gotowych obiektów typu Pracownik
        /// </summary>
        /// <returns>Lista 3 pracowników z wypełnionymi danymi</returns>
        public static List<Pracownik> PrzygotujDane()
        {
            List<Pracownik> lista = new List<Pracownik>
            {
                new Pracownik
                (
                    "Jan",
                    "Kowalski",
                    true,
                    false,
                    false,
                    false,
                    false
                ),
                new Pracownik
                (
                    "Jan",
                    "Nowak",
                    true,
                    true,
                    false,
                    true,
                    false
                ),
                new Pracownik
                (
                    "Piotr",
                    "Kwiatkowski",
                    false,
                    false,
                    true,
                    true,
                    true
                )
            };
            return lista;
        }

        /// <summary>
        /// Kolekcja szablonów (prototypów) które można potem kopiować by stworzyć gotowy obiekt
        /// </summary>
        public static Dictionary<string, Pracownik> prototypy = new Dictionary<string, Pracownik>
        {
            {"PracownikLaboratorium", new Pracownik("","",true,false,false,false,false)},
            {"PracownikSprzedazy",new Pracownik("","",false,false,false,true,false)},
            {"Menedzer",new Pracownik("","",true,true,true,true,false)},
            {"Szef",new Pracownik("","",true,true,true,true,true)}
        };

        /// <summary>
        /// Domyślny konstruktor wymagający podania wszystkich danych
        /// </summary>
        public Pracownik()
        {
            Console.WriteLine("Podaj Imię:");
            imie = Console.ReadLine();
            Console.WriteLine("Podaj Nazwisko:");
            nazwisko = Console.ReadLine();

            Console.WriteLine("Podaj uprawnienia do sali A1 (0/1):");
            salaA1 = Console.ReadLine() == "1";
            Console.WriteLine("Podaj uprawnienia do sali A2 (0/1):");
            salaA2 = Console.ReadLine() == "1";
            Console.WriteLine("Podaj uprawnienia do sali A3 (0/1):");
            salaA3 = Console.ReadLine() == "1";
            Console.WriteLine("Podaj uprawnienia do sali A4 (0/1):");
            salaA4 = Console.ReadLine() == "1";
            Console.WriteLine("Podaj uprawnienia do sali A5 (0/1):");
            salaA5 = Console.ReadLine() == "1";
        }

        /// <summary>
        /// Wewnętrzny konstruktor inicjalizujący pracowników do przykładów. Nie jest częścią wzorca.
        /// </summary>
        private Pracownik(string _imie, string _nazwisko, bool a1, bool a2, bool a3, bool a4, bool a5)
        {
            imie = _imie;
            nazwisko = _nazwisko;
            salaA1 = a1;
            salaA2 = a2;
            salaA3 = a3;
            salaA4 = a4;
            salaA5 = a5;
        }

        /// <summary>
        /// Funkcja tworząca pracownika na podstawie prototypu. Wykorzystuje Shallow Copy
        /// </summary>
        /// <param name="prototyp">Nazwa szablonu do skopiowania.</param>
        /// <returns>Gotowy obiekt typu Pracownik</returns>
        public static Pracownik StworzPracownika(string prototyp)
        {
            Pracownik nowy = prototypy[prototyp].Kopiuj();

            Console.WriteLine("Podaj Imię:");
            nowy.imie = Console.ReadLine();
            Console.WriteLine("Podaj Nazwisko:");
            nowy.nazwisko = Console.ReadLine();

            return nowy;
        }

        /// <summary>
        /// Funkcja wypisująca dane pracownika na ekranie.
        /// </summary>
        /// <param name="pracownik">Obiekt do wypisania</param>
        public static void WypiszPracownika(Pracownik pracownik)
        {
            Console.WriteLine("Pracownik:");
            Console.WriteLine(pracownik.imie + " " + pracownik.nazwisko);
            Console.WriteLine("Uprawnienia:");
            Console.WriteLine("Sala A1: " + (pracownik.salaA1 ? "tak" : "nie"));
            Console.WriteLine("Sala A2: " + (pracownik.salaA2 ? "tak" : "nie"));
            Console.WriteLine("Sala A3: " + (pracownik.salaA3 ? "tak" : "nie"));
            Console.WriteLine("Sala A4: " + (pracownik.salaA4 ? "tak" : "nie"));
            Console.WriteLine("Sala A5: " + (pracownik.salaA5 ? "tak" : "nie"));
            Console.WriteLine();
        }
    }

}
