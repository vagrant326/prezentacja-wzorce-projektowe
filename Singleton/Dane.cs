﻿using System.Collections.Generic;
using System.Threading;

namespace Singleton
{
    /// <summary>
    /// Klasa operująca na bazie danych
    /// </summary>
    class Dane
    {
        #region Wymagane przez Singleton
        /// <summary>
        /// Obiekt przechowujący faktyczne dane. W ogólnych schematach wzorca nazywany poprostu Singleton`em.
        /// </summary>
        private static Dane instancja;

        /// <summary>
        /// Właściwość służąca do odczytu danych
        /// </summary>
        static public Dane Instancja
        {
            get
            {
                // Jeśli dane nie zostały jeszcze pobrane
                if (instancja == null)
                {
                    // Pobierz dane z bazy
                    instancja = new Dane();
                }
                // Zwróć dane
                return instancja;
            }
        }

        /// <summary>
        /// Prywatny konstruktor - Singleton nie pozwala użytkownikowi klasy na samodzielne tworzenie obiektu
        /// </summary>
        private Dane()
        {
            listaStacji = AktualizujStacje();
        }
        #endregion

        #region Wymagane do przykładu
        /// <summary>
        /// Pole przechowujące dane o stacjach w przypadku nie używania wzorca Singleton
        /// </summary>
        static private List<Stacja> listaStacji; 

        /// <summary>
        /// Funkcja pobierająca aktualne dane
        /// </summary>
        /// <returns>Zaktualizowane dane o stacjach</returns>
        public static List<Stacja> AktualizujStacje()
        {
            List<Stacja> wszystkieStacje = new List<Stacja>();

            // Bardzo czasochłonne zapytanie
            Thread.Sleep(5000); 

            return wszystkieStacje;
        }

        /// <summary>
        /// Funkcja zwracająca dane o stacjach w przypadku nie używania wzorca Singleton
        /// </summary>
        /// <returns>Dane o stacjach</returns>
        public List<Stacja> ZwrocListe()
        {
            return listaStacji;
        }
        #endregion
    }
}
