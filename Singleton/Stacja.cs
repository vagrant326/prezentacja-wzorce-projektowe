﻿namespace Singleton
{
    /// <summary>
    /// Przykładowa klasa, mapująca obiekt z fikcyjnej bazy danych
    /// </summary>
    class Stacja
    {
        public string nazwa;
        public string adres;
    }
}
