﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Singleton
{
    public partial class Form1 : Form
    {
        /* W tym przykładzie używamy wzorca Singleton ponieważ wczytanie danych o stacjach służb publicznych
         * wymaga zbyt dużo czasu zakładając że aplikacja jest używana w Centrum Powiadamiania Ratunkowego.
         */
        public Form1()
        {
            InitializeComponent();
            pictureBox1.Image = Image.FromFile(@"../../mapa_pl.jpg");
            pictureBox2.Image = Image.FromFile(@"../../mapa_pl.jpg");
            pictureBox3.Image = Image.FromFile(@"../../mapa_pl.jpg");
        }

        /// <summary>
        /// Funkcja uruchamiająca się w momencie zmiany zakładki w oknie
        /// </summary>
        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {
            // Jeśli wybrano zakładkę z buttonami...
            if (tabControl1.SelectedIndex<1)
                return;

            // W przeciwnym wypadku...
            // --- Wzorzec Singleton ---
            
            //List<Stacja> stacje = Dane.Instancja.ZwrocListe();

            // --- Zwykła inicjalizacja ---

            List<Stacja> stacje = Dane.AktualizujStacje();

            // Następnie...
            // Filtruj stacje po typie służb...
            // Wyświetl dane na mapce...
        }
        
        // Funkcje uruchamiane po naciśnięciu button`ów. Zmieniają tekst w label2.
        #region Przyciski
        private void button1_Click(object sender, EventArgs e)
        {
            label2.Text = "Wysłano Policję na adres " + textBox1.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label2.Text = "Wysłano Straż Pożarną na adres " + textBox1.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label2.Text = "Wysłano Pogotowie na adres " + textBox1.Text;
        }
        #endregion
    }
}
