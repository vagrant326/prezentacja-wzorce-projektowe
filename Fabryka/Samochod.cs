﻿using System;

namespace Fabryka
{
    /// <summary>
    /// Klasa abstrakcyjna tworząca interfejs który musi zostać zaimplemetowany
    /// przez każdy obiekt typu Samochod. We wzorcu nazywany Product.
    /// </summary>
    public abstract class Samochod
    {
        public string marka = "Suzuki";
        /// <summary>
        /// Jedyne pole które będzie ulegać zmianom
        /// </summary>
        public abstract string model { get; }
        public int iloscKol = 4;
        public int rocznik = DateTime.Now.Year;
    }
}
