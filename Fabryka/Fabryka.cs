﻿namespace Fabryka
{
    /// <summary>
    /// Klasa abstrakcyjna tworząca interfejs który musi zostać zaimplementowany przez
    /// każdy obiekt typu Fabryka. We wzorcu nazywany Creator lub Factory.
    /// </summary>
    public abstract class Fabryka
    {
        /// <summary>
        /// Funkcja zwracająca wybrany obiekt implemetujący interfejs Samochod
        /// </summary>
        /// <param name="nazwa">Nazwa produktu</param>
        /// <returns>Gotowy do użycia obiekt typu Samochód</returns>
        public abstract Samochod Wyprodukuj(string nazwa);
    }
}
