﻿using System;

namespace Fabryka
{
    /* Plik zawierający 2 przykładowe implementacje interfejsu IFabryka.
     * Każda z nich pozwala wybrać spośród 2 modeli samochodów implementujących interfejs Samochod.
     * We wzorcu nazywane Concrete Factory lub Concrete Creator.
     */
    class MagyarSuzukiCorp : Fabryka
    {
        public override Samochod Wyprodukuj(string nazwa)
        {
            if (nazwa == "Vitara")
            {
                // Wyprodukuj Suzuki Vitara
                return new Vitara();
            }
            if (nazwa == "Wagon R+")
            {
                // Wyprodukuj Suzuki Wagon R+
                return new WagonRPlus();
            }
            Console.WriteLine("Zla nazwa");
            return null;
        }
    }

    class CambodiaSuzukiMotor : Fabryka
    {
        public override Samochod Wyprodukuj(string nazwa)
        {
            if (nazwa == "Samurai")
            {
                return new Samurai();
            }
            if ( nazwa == "Swift")
            {
                return new Swift();
            }
            Console.WriteLine("Zla nazwa");
            return null;
        }
    }
}
