﻿using System;
namespace Fabryka
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Używamy tutaj wzorca fabryka ponieważ służy on do tworzenia obiektów
             * z jednej rodziny typów (implementujących ten sam interfejs). Ilość 
             * fabryk nie ma większego znaczenia.
             */

            // Utwórz nowy obiekt Concrete Creator
            Fabryka fab = new MagyarSuzukiCorp();

            // Wyślij zamówienie na nowy produkt do fabryki fab
            Samochod auto = Producent.WyprodukujSamochod(fab);


            // Zmień fabrykę
            fab = new CambodiaSuzukiMotor();

            // Utwórz nowy produkt
            auto = Producent.WyprodukujSamochod(fab);

            Console.ReadLine();
        }
    }
}
