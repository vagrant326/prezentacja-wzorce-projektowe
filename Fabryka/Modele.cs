﻿namespace Fabryka
{
    /* Plik zawierający różne modele Suzuki implementujące interfejs Samochód.
     * Zmienia się tylko przykładowe pole ale bez problemu można zmieniać ich wiele.
     */
    class WagonRPlus : Samochod
    {
        public override string model
        {
            get { return "Wagon R+"; }
        }
    }

    class Vitara : Samochod
    {
        public override string model
        {
            get { return "Vitara"; }
        }
    }
    class Samurai : Samochod
    {
        public override string model
        {
            get { return "Samurai"; }
        }
    }
    class Swift : Samochod
    {
        public override string model
        {
            get { return "Swift"; }
        }
    }
}
