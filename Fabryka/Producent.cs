﻿using System;

namespace Fabryka
{
    /// <summary>
    /// Klasa pośrednicząca między klientem a fabryką.
    /// Wybór produktu może być przeprowadzany na dowolny sposób - if, switch, enum itp.
    /// na podstawie stringa, kontrolki WinForms, bazy danych itp.
    /// </summary>
    class Producent
    {
        /// <summary>
        /// Funkcja pozwalająca wybrać jaki produkt ma zostać wytworzony.
        /// </summary>
        /// <param name="fabryka">Concrete Factory wybrany do wytworzenia obiektu</param>
        /// <returns>Wybrany produkt gotowy do użycia</returns>
        public static Samochod WyprodukujSamochod(Fabryka fabryka)
        {
            Console.WriteLine("Fabryka " + fabryka.GetType().Name);
            Console.WriteLine("Podaj nazwę modelu:");
            string nazwa = Console.ReadLine();
            Samochod samochod = fabryka.Wyprodukuj(nazwa);
            Console.WriteLine("Wyprodukowano " + samochod.marka + " " + samochod.model);
            Console.WriteLine();
            return samochod;
        }
    }
}
