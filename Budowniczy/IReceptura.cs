﻿namespace Budowniczy
{
    /// <summary>
    /// Interfejs opisujący proces produkcji piwa. 
    /// Musi być zaimplemetowany przez każdą recepturę.
    /// We wzorcu opisywany jako Abstract Builder.
    /// </summary>
    interface IReceptura
    {
        void SrutowanieSlodu();
        void ZacieranieSruty();
        void FiltracjaZacieru();
        void GotowanieBrzeczki();
        void OddzielanieOsadu();
        void SchladzanieBrzeczki();
        void NatlenianieBrzeczki();
        void FermentacjaBrzeczki();
        void FiltracjaBrzeczki();
        void Lezakowanie();
        void Filtracja();
        void Pasteryzacja();
        void Rozlew();
        /// <summary>
        /// Funkcja zwracająca produkt w aktualnym stanie.
        /// </summary>
        /// <returns>Produkt procesu według wybranej receptury</returns>
        Piwo ZwrocProdukt();
    }
}
