﻿namespace Budowniczy
{
    /// <summary>
    /// Klasa przykładowa, nie posiada żadnej funkcjonalności. 
    /// Wzorzec może używać i zmieniać złożone (referencyjne) pola obiektu.
    /// </summary>
    class SkladChemiczny
    {
        /// <summary>
        /// Pusta funkcja.
        /// </summary>
        public void Zmien(string skladnik, double wartosc)
        {
            return;
        }
    }
}
