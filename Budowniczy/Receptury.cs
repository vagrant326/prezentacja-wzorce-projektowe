﻿using System;

namespace Budowniczy
{
    /* Plik zawierający przykładowe implementacje interfejsu IReceptura.
     * Najważniejszy w nich jest fakt, że każda receptura implementuje wszystkie funkcje
     * ale każda z nich robi to na swój własny sposób.
     * We wzorcu receptury określone są jako Concrete Builder.
     * Receptury są zmyślone. Nie próbujcie tego w domu :-)
     */
    class PiwoJasne : IReceptura
    {
        private Piwo piwo = new Piwo();
        public void SrutowanieSlodu()
        {
            piwo.opis += "Jeczmienne, ";
        }

        public void ZacieranieSruty()
        {
            piwo.temperatura = 60;
            piwo.sklad.Zmien("Woda", 100);
            piwo.sklad.Zmien("Enzymy", 5);
        }

        public void FiltracjaZacieru()
        {
            piwo.Filtruj(100);
        }

        public void GotowanieBrzeczki()
        {
            piwo.sklad.Zmien("AromatChmiel", 0.6);
            piwo.temperatura = 100;
            piwo.Czekaj(1);
        }

        public void OddzielanieOsadu()
        {
            piwo.Filtruj(100);
        }

        public void SchladzanieBrzeczki()
        {
            piwo.temperatura = 20;
        }

        public void NatlenianieBrzeczki()
        {
            piwo.sklad.Zmien("Tlen",150);
        }

        public void FermentacjaBrzeczki()
        {
            piwo.sklad.Zmien("Tlen", 0);
            piwo.sklad.Zmien("CO2", 150);
            piwo.iloscAlkoholu = 2.3;
            piwo.Czekaj(24);
        }

        public void FiltracjaBrzeczki()
        {
            piwo.Filtruj(100);
        }

        public void Lezakowanie()
        {
            piwo.Czekaj(336);
            piwo.opis += "Lezakujace 2 tygodnie, ";
        }

        public void Filtracja()
        {
            piwo.Filtruj(100);
            piwo.opis += "Czyste, ";
        }

        public void Pasteryzacja()
        {
            piwo.temperatura = 100;
            piwo.opis += "Pasteryzowane, ";
        }

        public void Rozlew()
        {
            piwo.temperatura = 40;
            piwo.nazwa = "Klasyk";
            piwo.gatunek = "Jasne";
            piwo.rocznik = DateTime.Now.Year;
        }

        public Piwo ZwrocProdukt()
        {
            return piwo;
        }
    }

    class Niepasteryzowane : IReceptura
    {
        private Piwo piwo = new Piwo();
        public void SrutowanieSlodu()
        {
            piwo.opis += "Jeczmienne, ";
        }

        public void ZacieranieSruty()
        {
            piwo.temperatura = 60;
            piwo.sklad.Zmien("Woda", 100);
            piwo.sklad.Zmien("Enzymy", 5);
        }

        public void FiltracjaZacieru()
        {
            piwo.Filtruj(100);
        }

        public void GotowanieBrzeczki()
        {
            piwo.sklad.Zmien("AromatChmiel", 0.6);
            piwo.temperatura = 100;
            piwo.Czekaj(1);
        }

        public void OddzielanieOsadu()
        {
            piwo.Filtruj(100);
        }

        public void SchladzanieBrzeczki()
        {
            piwo.temperatura = 20;
        }

        public void NatlenianieBrzeczki()
        {
            piwo.sklad.Zmien("Tlen", 150);
        }

        public void FermentacjaBrzeczki()
        {
            piwo.sklad.Zmien("Tlen", 0);
            piwo.sklad.Zmien("CO2", 150);
            piwo.iloscAlkoholu = 3.5;
            piwo.Czekaj(48);
        }

        public void FiltracjaBrzeczki()
        {
            piwo.Filtruj(100);
        }

        public void Lezakowanie()
        {
            piwo.Czekaj(3*7*24);
            piwo.opis += "Lezakujace 3 tygodnie, ";
        }

        public void Filtracja()
        {
            piwo.Filtruj(100);
            piwo.opis += "Czyste, ";
        }

        public void Pasteryzacja()
        {
            piwo.temperatura = 20;
            piwo.opis += "Niepasteryzowane, ";
        }

        public void Rozlew()
        {
            piwo.temperatura = 40;
            piwo.nazwa = "Niepasteryzowane";
            piwo.gatunek = "Jasne";
            piwo.rocznik = DateTime.Now.Year;
        }

        public Piwo ZwrocProdukt()
        {
            return piwo;
        }
    }

    class Metne : IReceptura
    {
        private Piwo piwo = new Piwo();
        public void SrutowanieSlodu()
        {
            piwo.opis += "Jeczmienne, ";
        }

        public void ZacieranieSruty()
        {
            piwo.temperatura = 60;
            piwo.sklad.Zmien("Woda", 100);
            piwo.sklad.Zmien("Enzymy", 5);
        }

        public void FiltracjaZacieru()
        {
            piwo.Filtruj(100);
        }

        public void GotowanieBrzeczki()
        {
            piwo.sklad.Zmien("AromatChmiel", 0.6);
            piwo.temperatura = 100;
            piwo.Czekaj(1);
        }

        public void OddzielanieOsadu()
        {
            piwo.Filtruj(100);
        }

        public void SchladzanieBrzeczki()
        {
            piwo.temperatura = 20;
        }

        public void NatlenianieBrzeczki()
        {
            piwo.sklad.Zmien("Tlen", 150);
        }

        public void FermentacjaBrzeczki()
        {
            piwo.sklad.Zmien("Tlen", 0);
            piwo.sklad.Zmien("CO2", 150);
            piwo.iloscAlkoholu = 4.5;
            piwo.Czekaj(24);
        }

        public void FiltracjaBrzeczki()
        {
            piwo.Filtruj(100);
        }

        public void Lezakowanie()
        {
            piwo.Czekaj(336);
            piwo.opis += "Lezakujace 2 tygodnie, ";
        }

        public void Filtracja()
        {
            piwo.Filtruj(50);
            piwo.opis += "Metne, ";
        }

        public void Pasteryzacja()
        {
            piwo.temperatura = 100;
            piwo.opis += "Pasteryzowane, ";
        }

        public void Rozlew()
        {
            piwo.temperatura = 40;
            piwo.nazwa = "Mist";
            piwo.gatunek = "Metne";
            piwo.rocznik = DateTime.Now.Year;
        }

        public Piwo ZwrocProdukt()
        {
            return piwo;
        }
    }

    class Ciemne : IReceptura
    {
        private Piwo piwo = new Piwo();
        public void SrutowanieSlodu()
        {
            piwo.opis += "Jeczmienno-Pszeniczne, ";
        }

        public void ZacieranieSruty()
        {
            piwo.temperatura = 90;
            piwo.sklad.Zmien("Woda", 100);
            piwo.sklad.Zmien("Enzymy", 5);
        }

        public void FiltracjaZacieru()
        {
            piwo.Filtruj(100);
        }

        public void GotowanieBrzeczki()
        {
            piwo.sklad.Zmien("AromatChmiel", 0.6);
            piwo.temperatura = 100;
            piwo.Czekaj(1);
        }

        public void OddzielanieOsadu()
        {
            piwo.Filtruj(100);
        }

        public void SchladzanieBrzeczki()
        {
            piwo.temperatura = 20;
        }

        public void NatlenianieBrzeczki()
        {
            piwo.sklad.Zmien("Tlen", 150);
        }

        public void FermentacjaBrzeczki()
        {
            piwo.sklad.Zmien("Tlen", 0);
            piwo.sklad.Zmien("CO2", 150);
            piwo.iloscAlkoholu = 2.3;
            piwo.Czekaj(24);
        }

        public void FiltracjaBrzeczki()
        {
            piwo.Filtruj(100);
        }

        public void Lezakowanie()
        {
            piwo.Czekaj(336);
            piwo.opis += "Lezakujace 2 tygodnie, ";
        }

        public void Filtracja()
        {
            piwo.Filtruj(90);
            piwo.opis += "Czyste, ";
        }

        public void Pasteryzacja()
        {
            piwo.temperatura = 100;
            piwo.opis += "Pasteryzowane, ";
        }

        public void Rozlew()
        {
            piwo.temperatura = 40;
            piwo.nazwa = "Noc";
            piwo.gatunek = "Ciemne";
            piwo.rocznik = DateTime.Now.Year;
        }

        public Piwo ZwrocProdukt()
        {
            return piwo;
        }
    }
}
