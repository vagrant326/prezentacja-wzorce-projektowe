﻿using System;

namespace Budowniczy
{
    class Program
    {
        static void Main(string[] args)
        {
            /* W tym przykładzie używamy wzorca Budowniczy ponieważ przygotowywany produkt ma wiele wersji
             * a jego produkcja ma sztywno ustalony schemat. Wersje różnią się tylko detalami w trakcie produkcji.
             */

            // Stwórz nadzorcę
            Warzelnia browar = new Warzelnia();

            // Przydziel mu Konkretnego Budowniczego
            browar.UstalRecepture(new PiwoJasne());

            // Buduj()
            browar.Produkuj();

            // Zwróc gotowy produkt
            Piwo produkt = browar.Zwroc();

            // Wypisz produkt na ekranie
            produkt.Wypisz();


            // Zmień Budowniczego
            browar.UstalRecepture(new Ciemne());

            // Buduj()
            browar.Produkuj();

            // Zwróć gotowy produkt
            produkt = browar.Zwroc();

            // Wypisz produkt na ekranie
            produkt.Wypisz();

            Console.ReadLine();
        }
        //Piwo piwo = new PiwoJasne().Mieszaj().Gotuj().Filtruj().Zwroc();
    }
}
