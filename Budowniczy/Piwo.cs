﻿using System;

namespace Budowniczy
{
    /// <summary>
    /// Klasa modelująca piwo w procesie produkcji. 
    /// We wzorcu najczęściej opisywany jako Produkt.
    /// </summary>
    class Piwo
    {
        public string nazwa;
        public int temperatura;
        public SkladChemiczny sklad;
        public double iloscAlkoholu;
        public int rocznik;
        public string gatunek;
        public string opis;

        /// <summary>
        /// Domyślny konstruktor. Inicjalizuje wyłącznie pole referencyjne.
        /// </summary>
        public Piwo()
        {
            sklad = new SkladChemiczny();
        }

        /// <summary>
        /// Funkcja wypisująca pola obiektu na ekranie
        /// </summary>
        public void Wypisz()
        {
            Console.WriteLine("Piwo " + nazwa + ":");
            Console.WriteLine(iloscAlkoholu + "% alk.,");
            Console.WriteLine("wyprodukowano w " + rocznik + " roku,");
            Console.WriteLine("z gatunku " + gatunek + ".");
            Console.WriteLine("Opis: " + opis);
        }

        /// <summary>
        /// Pusta funkcja zwiększająca czytelność przykładu. 
        /// Obrazuje sytuacje gdy proces produkcji wymaga dłuższego czasu.
        /// </summary>
        /// <param name="godziny">Ile godzin ma trwać proces</param>
        public void Czekaj(int godziny)
        {
            return;
        }

        /// <summary>
        /// Pusta funkcja zwiększająca czytelność przykładu. 
        /// Obrazuje sytuacje gdy piwo ma zostać oddzielone od osadów i zawiesin.
        /// </summary>
        /// <param name="procent">Ile procent zanieczyszczeń ma zostać odfiltrowanych</param>
        public void Filtruj(int procent)
        {
            return;
        }
    }
}
