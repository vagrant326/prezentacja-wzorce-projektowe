﻿namespace Budowniczy
{
    /// <summary>
    /// Klasa wykonująca proces warzenia według podanej receptury.
    /// We wzorcu opisywana jako Director.
    /// </summary>
    class Warzelnia
    {
        /// <summary>
        /// Pole przechowujące recepturę (Budowniczego) według której będzie produkowane piwo.
        /// </summary>
        private IReceptura receptura;

        /// <summary>
        /// Funkcja zmieniająca recepturę. Można też zmienić pole receptura na właściwość bez {set;}
        /// </summary>
        /// <param name="_receptura">Receptura implementująca interfejs IReceptura</param>
        public void UstalRecepture(IReceptura _receptura)
        {
            receptura = _receptura;
        }

        /// <summary>
        /// Funkcja realizująca proces warzenia.
        /// We wzorcu często opisywana jako Build() lub Construct()
        /// </summary>
        public void Produkuj()
        {
            receptura.SrutowanieSlodu();
            receptura.ZacieranieSruty();
            receptura.FiltracjaZacieru();
            receptura.GotowanieBrzeczki();
            receptura.OddzielanieOsadu();
            receptura.SchladzanieBrzeczki();
            receptura.NatlenianieBrzeczki();
            receptura.FermentacjaBrzeczki();
            receptura.FiltracjaBrzeczki();
            receptura.Lezakowanie();
            receptura.Filtracja();
            receptura.Pasteryzacja();
            receptura.Rozlew();
        }

        /// <summary>
        /// Funkcja zwracająca obiekt produktu z klasy receptura
        /// </summary>
        /// <returns>Produkt</returns>
        public Piwo Zwroc()
        {
            return receptura.ZwrocProdukt();
        }
    }
}
